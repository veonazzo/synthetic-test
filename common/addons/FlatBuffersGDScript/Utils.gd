extends Reference


static func fill_array(array: Array, from_index: int, to_index: int, val):
	if from_index > to_index:
		print("from_index > to_index")
		return
		
	if from_index < 0:
		print("from_index < 0")
		return
		
	if to_index > array.size():
		print("to_index > array.size()")
		return
	
	for i in range(from_index, to_index):
		array[i] = val


static func create_valued_array(size: int, val) -> Array:
	var array: Array
	array.resize(size)
	for i in range(0, size):
		array[i] = val
	return array


static func get_val(flags, buffer: StreamPeerBuffer, index: int):
	buffer.seek(index)
	
	var value = null
	match flags.name:
		"bool":
			value = buffer.get_8() == 1
		"int8":
			value = buffer.get_8()
		"int16":
			value = buffer.get_16()
		"int32":
			value = buffer.get_32()
		"int64":
			value = buffer.get_64()
		"uint8":
			value = buffer.get_u8()
		"uint16":
			value = buffer.get_u16()
		"uint32":
			value = buffer.get_u32()
		"uint64":
			value = buffer.get_u64()
		"float32":
			value = buffer.get_float()
		"float64":
			value = buffer.get_double()
	return value

