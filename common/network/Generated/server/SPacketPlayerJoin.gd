# automatically generated by the FlatBuffers compiler, do not modify

# namespace: server

class_name SPacketPlayerJoin extends Table

static func get_root_as(buffer: StreamPeerBuffer,
								obj: SPacketPlayerJoin,
								offset: int = -1) -> SPacketPlayerJoin:
	buffer.set_big_endian(false)
	if offset == -1:
		offset = buffer.get_position()

	buffer.seek(offset)
	var pos: int = buffer.get_u32()

	obj._assign(buffer, pos + offset)
	return obj

func _assign(buffer: StreamPeerBuffer, offset: int) -> void:
	_reset(buffer, offset)

func user_id(obj: UUID = UUID.new()) -> UUID:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(4))
	if o != 0:
		var x: int = self._pos + o
		obj._assign(self._buffer, x)
		return obj
	return null

func username() -> String:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(6))
	if o != 0:
		return string(self._pos + o)
	return ""

func display_name() -> String:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(8))
	if o != 0:
		return string(self._pos + o)
	return ""

func rotation(obj: EulerAngles = EulerAngles.new()) -> EulerAngles:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(10))
	if o != 0:
		var x: int = self._pos + o
		obj._assign(self._buffer, x)
		return obj
	return null

func position(obj: Vec3f = Vec3f.new()) -> Vec3f:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(12))
	if o != 0:
		var x: int = self._pos + o
		obj._assign(self._buffer, x)
		return obj
	return null

static func create_spacketplayerjoin(builder: FlatBuffersBuilder,
		user_id_offset: int,
		username_offset: int,
		display_name_offset: int,
		rotation_offset: int,
		position_offset: int) -> int:
	start(builder);
	add_position(builder, position_offset)
	add_rotation(builder, rotation_offset)
	add_display_name(builder, display_name_offset)
	add_username(builder, username_offset)
	add_user_id(builder, user_id_offset)
	return end(builder);

static func start(builder: FlatBuffersBuilder) -> void:
	builder.start_table(5)

static func add_user_id(builder: FlatBuffersBuilder, user_id_offset: int) -> void:
	builder.prepend_struct_slot(0, user_id_offset, 0)

static func add_username(builder: FlatBuffersBuilder, username_offset: int) -> void:
	builder.prepend_UOffsetTRelative_slot(1, username_offset, 0)

static func add_display_name(builder: FlatBuffersBuilder, display_name_offset: int) -> void:
	builder.prepend_UOffsetTRelative_slot(2, display_name_offset, 0)

static func add_rotation(builder: FlatBuffersBuilder, rotation_offset: int) -> void:
	builder.prepend_struct_slot(3, rotation_offset, 0)

static func add_position(builder: FlatBuffersBuilder, position_offset: int) -> void:
	builder.prepend_struct_slot(4, position_offset, 0)

static func end(builder: FlatBuffersBuilder) -> int:
	return builder.end_table()


