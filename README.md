This is only a test project: this explains some code comments and commits in Italian, as it allows us to speed up the process (we're all Italians). The main one will be in English, of course :D

## Credits
All graphic resources are under [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).  
Male 3D model by Zughy
