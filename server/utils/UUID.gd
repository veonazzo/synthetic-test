extends Reference
class_name UUIDClass

var most_sig_bits: int = 0
var least_sig_bits: int = 0

func _init(string_uuid: String):
	string_uuid = string_uuid.replace("-", "")
	
	if string_uuid.length() == 32:
		for i in range(0, 16, 4):
			most_sig_bits |= ("0x%s" % [string_uuid.substr(i, 4)]).hex_to_int()
			if i < 16 - 4:
				most_sig_bits <<= 16
			
		for i in range(16, 32, 4):
			least_sig_bits |= ("0x%s" % [string_uuid.substr(i, 4)]).hex_to_int()
			if i < 32 - 4:
				least_sig_bits <<= 16

func _to_string():
	return ("%s-%s-%s-%s-%s" % [
		_digits(most_sig_bits >> 32, 8),
		_digits(most_sig_bits >> 16, 4),
		_digits(most_sig_bits, 4),
		_digits(least_sig_bits >> 48, 4),
		_digits(least_sig_bits, 12)
	])

static func _digits(val: int, digits: int) -> String:
	var hi: int = 1 << (digits * 4)
	return ("%x" % [hi | (val & (hi - 1))]).substr(1)
