extends Node


var categories : Dictionary = {
	"weapons": true,
	"movement": false,
	"camera": false,
	"damage": false,
	"player": false,
	"bullets": false,
	"raycast_utils": false,
	"physics": false,
	"input": false
}


func Print(category: String, text) -> void:
	if is_category_active(category):
		print("[" + category + "] " + str(text))
	elif not categories.has(category):
		printerr(category + " is not a valid debug category")


func print_line(category: String, text) -> void:
	if is_category_active(category):
		print("\n")
		Print(category, text)


func is_category_active(category: String):
	return categories.has(category) and categories[category]
