extends Spatial


func raycast(origin_pos: Vector3, target_pos: Vector3, exclusions: Array) -> Dictionary:
	var space_state := get_world().direct_space_state
	var result: Dictionary

	result = space_state.intersect_ray(origin_pos, target_pos, exclusions)

	return result


func raycast_pierce(origin_pos: Vector3, target_pos: Vector3, exclusions: Array) -> Dictionary:
	var space_state := get_world().direct_space_state
	var exclude = exclusions.duplicate(true)
	var hits = []
	while(true):
		var result = space_state.intersect_ray(origin_pos, target_pos, exclude)
		if(result.empty()):
			break
		hits.append(result)
		exclude.append(result.collider)
	return hits


func grid_raycast(origin: Transform, target: Transform, ray_count: int, grid_size: Vector2, exclusions: Array) -> Dictionary:
	var result: Dictionary

	var origin_pos := origin.origin
	var target_pos := target.origin
	var facing_target_basis := origin.looking_at(target.origin, Vector3.UP).basis

	var initial_offset: Vector3 = facing_target_basis.x * grid_size.x + facing_target_basis.y * grid_size.y
	var ray_start: Vector3 = origin_pos - initial_offset
	var ray_end: Vector3 = target_pos - initial_offset

	var pos_step_x: Vector3 = grid_size.x * 2 / ray_count * facing_target_basis.x
	var pos_step_y: Vector3 = grid_size.y * 2 / ray_count * facing_target_basis.y

	for x in ray_count:
		for y in ray_count:
			if result.has("position"):
				break

			result = raycast(ray_start, ray_end, exclusions)

			if Debug.is_category_active("raycast_utils"):
				DrawLine3d.DrawLine(ray_start, ray_end, Color.red, 0)

			ray_start += pos_step_y
			ray_end += pos_step_y

		# Faccio tornare il raggio all'altezza minima.
		ray_start -=  pos_step_y * ray_count
		ray_end -= pos_step_y * ray_count

		ray_start += pos_step_x
		ray_end += pos_step_x

	if result.has("position"):
		Debug.print_line("raycast_utils", "Grid collision pos: " + str(result["position"]))
		return result
	else:
		Debug.print_line("raycast_utils", "Grid found no collisions")
		return {}
