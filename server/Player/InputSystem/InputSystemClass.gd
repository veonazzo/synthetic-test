class_name InputSystemClass


var _input_systems:Array
enum Actions {
	p_1_slide,
	u_1_0_primary_fire,
	p_1_0_jump,
	p_1_0_forward,
	p_2_300_dash,
	p_1_0_backward,
	p_1_0_right,
	p_1_0_left,
	p_1_0_shift,
	u_1_0_reload,
	u_1_0_right_click,
}


"""
Stati: 0 Just Pressed, 1 Pressing, 2 Just Released
_inputs = [
	{"walking":{
		states: [
			{ pressed: false, func: "func" },
			{ just_pressed: false, func: "func" },
			{ just_released: false, func: "func" }
		],
		required_clicks: 1, // just_pressed
		max_click_delay: 300, // just_pressed
		clicks: 0,
		last_timestamp: 15654 // just_pressed
	}},

	{"forward":{
		states: [
			{ pressed: true, func: null },
		]
		required_clicks: 1, // just_pressed
		max_click_delay: 300, // just_pressed
		clicks: 0,
		last_timestamp: 0 // just_pressed
	}

	{"running": {
		states: [
			{ pressed: false, func: "func" },
			{ just_pressed: false, func: "func" },
			{ just_released: false, func: "func" }
		],
		required_clicks: 2, // just_pressed
		max_click_delay: 300, // just_pressed
		clicks: 0,
		last_timestamp: 15654 // just_pressed
	}}
]
"""


func _init() -> void:

	_input_systems.append(InputSystemActions.new("u"))
	_input_systems.append(InputSystemActions.new("p"))

	var parsed_actions_names:Dictionary = {}
	var actions = InputMap.get_actions()

	for action in actions:
		parsed_actions_names[action] = action.split("_")

	for parsed_action_key in parsed_actions_names:
		var parsed_action_name = parsed_actions_names[parsed_action_key]
		if parsed_action_name.size() < 4:
			_input_systems[1].add_action(InputSystemAction.new(parsed_action_key, 1, 0, "p", false))

		else:
			var i = 0

			while (parsed_action_name[0] != _input_systems[i].category):
				i += 1

			parsed_action_key.erase(0, parsed_action_name[1].length() + parsed_action_name[2].length() + 4)
			_input_systems[i].add_action(InputSystemAction.new(parsed_action_key, int(parsed_action_name[1]), int(parsed_action_name[2]), _input_systems[i].category))


func register_function(actions_array: Array, state_name: String, function_ref: FuncRef) -> void:
	for _input_system in _input_systems:
		for action in _input_system.actions:
			if (actions_array.has(action.name)):
				action.get_state(state_name).add_func(function_ref)


func process_actions(actions:CPacketActions) -> void:
	var enum_keys = Actions.keys()
	var keys = []
	var values = []
	for i in range(0, actions.input_actions_length()):
		var input_action = actions.input_actions(i)
		keys.append(input_action.name())
		values.append(input_action.states())
	
	# DA RENDERE PIÙ EFFICIENTE
	for i in range(0, _input_systems.size()):
		var _input_system = _input_systems[i]
		for _input_system_action in _input_system.actions:
			if enum_keys.has(_input_system_action.full_name): 
				var index = keys.find(Actions[_input_system_action.full_name])
				if index != -1 :
					_input_system_action.set_state_status(values[index])

		_execute_functions(i)


func _execute_functions(index:int) -> void:
	var current_time: int = OS.get_ticks_msec()
	for action in _input_systems[index].actions:
		action.process(current_time)


func get_state_status(action:String, state:String) -> bool:
	for _input_system in _input_systems:
		var action_buffer = _input_system.get_action_by_name(action)
		if  action_buffer != null:
			return action_buffer.get_state_status(state)

	return false


func is_just_pressed(action:String) -> bool:
	return self.get_state_status(action, "just_pressed")


func is_pressed(action:String) -> bool:
	return self.get_state_status(action, "pressed")


func is_just_released(action:String) -> bool:
	return self.get_state_status(action, "just_released")


func action_press(action:String) -> void:
	var action_buffer = null
	var i = 0
	while action_buffer == null:
		action_buffer = _input_systems[i].get_action_by_name(action)
		i += 1

	if action_buffer != null:
		action_buffer.set_state_status(0b011)


func action_release(action:String) -> void:
	var action_buffer = null
	var i = 0
	while action_buffer == null:
		action_buffer = _input_systems[i].get_action_by_name(action)
		i += 1

	if action_buffer != null:
		action_buffer.unset_state_status(0b011)
