class_name InputSystemAction


var name: String
var required_clicks: int
var max_click_delay: int

var clicks: int = 0
var last_timestamp: int = 0
var states: Array

var full_name: String

const just_pressed  = 0b001
const pressed       = 0b010
const just_released = 0b100


func _to_string() -> String:
	var final_string = "Name: %s, req clicks: %s, max ms: %s, full name: %s" % [self.name, self.required_clicks, self.max_click_delay, self.full_name]
	final_string += "\n    States: "
	for state in self.states:
		final_string += "\n        "
		final_string+= state._to_string()
	return final_string


func _init(name:String, required_clicks:int, max_click_delay:int, category:String, properly_named:bool =true) -> void:
	self.name = name
	self.required_clicks = required_clicks
	self.max_click_delay = max_click_delay
	if properly_named:
		self.full_name = category+"_"+str(required_clicks)+"_"+str(max_click_delay)+"_"+name
	else:
		self.full_name = name
	init_states()


func init_states() -> void:
	states.append(InputSystemState.new("just_pressed"))
	states.append(InputSystemState.new("pressed"))
	states.append(InputSystemState.new("just_released"))


func get_state(name:String):
	for state in states:
		if state.name == name:
			return state


func state_is_enabled(b, flag):
	return b & flag != 0


func set_state(b, flag):
		b = b|flag
		return b


func unset_state(b, flag):
		b = b & ~flag
		return b


func process(current_time:int) -> void:
	for state in states:
		if state.value:
			state.call_functions()


func get_state_status(name:String) -> bool:
	for state in states:
		if state.name == name:
			return state.value
	return false


func set_state_status(bitmask) -> void:
	for state in states:
		if (state_is_enabled(bitmask, get(state.name))):
			state.value = true
		else:
			state.value = false

func unset_state_status(bitmask) -> void:
	for state in states:
		if (state_is_enabled(bitmask, get(state.name))):
			state.value = false
			
