extends Node


var _input_system


func _init() -> void:
	_input_system = InputSystemClass.new()


func register_function(actions_array:Array, state_name:String, function_ref:FuncRef) -> void:
	_input_system.register_function(actions_array, state_name, function_ref)


func process_actions(actions:CPacketActions) -> void:
	_input_system.process_actions(actions)


func get_state_status(action:String, state:String) -> bool:
	return _input_system.get_state_status(action, state)


func is_just_pressed(action:String) -> bool:
	return get_state_status(action, "just_pressed")


func is_pressed(action:String) -> bool:
	return get_state_status(action, "pressed")


func is_just_released(action:String) -> bool:
	return get_state_status(action, "just_released")


func action_press(action:String) -> void:
	_input_system.action_press(action)


func action_release(action:String) -> void:
	_input_system.action_release(action)
