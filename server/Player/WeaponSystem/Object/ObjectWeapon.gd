extends ReloadWeapon
class_name ObjectWeapon

var bullet = null
var bullet_stats: Dictionary
var bullet_name: String


func init(player, type, owner_node, weapon_name, stats, id, extras=null) -> void:
	.init(player, type, owner_node, weapon_name, stats, id, extras)

	Debug.Print("weapons", "Test ObjectWeapon costructor")
	var path = "res://Player/WeaponSystem/Object/Bullets/"
	# Load the projectile config
	var config = ConfigFile.new()
	var err = config.load(path + stats["bullet_path"] + ".cfg")
	if err == OK:
		var stats_keys = config.get_section_keys("Stats")
		for key in stats_keys:
			bullet_stats[key] = config.get_value("Stats", key)
		# Load the projectile instance
		bullet_name = config.get_value("Details", "name")
		bullet = load(path + bullet_stats["scene"]+ ".tscn")


func _primary_use() -> bool:
	if ._primary_use():
		var transform_origin = shoot_origin.global_transform
		var origin = transform_origin.origin
		var shoot_dir = self.player.weapon_system.shoot_dir

		DrawLine3d.DrawRay(origin, shoot_dir * 1000, Color(0, 1, 0), 20)

		# Instance the scene
		var bullet_instance: Area = bullet.instance()
		# Call the constructor
		bullet_instance.init(player, bullet_name, bullet_stats.duplicate(true))
		# Add the scene to the world
		player.get_parent().add_child(bullet_instance)
		# Set the speed and starting point of the porjectile
		bullet_instance.velocity =  shoot_dir* bullet_stats["speed"]
		bullet_instance.transform = transform_origin
		# Turn the projectile in the right direction
		bullet_instance.look_at(origin + shoot_dir, Vector3.UP)

		return true

	return false
