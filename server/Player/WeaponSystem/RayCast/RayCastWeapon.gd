extends ReloadWeapon
class_name RayCastWeapon


# Constructor
func init(player, type, owner_node, weapon_name, stats, id, extras=null) -> void:
	.init(player, type, owner_node, weapon_name, stats, id, extras)

	Debug.Print("weapons", "Test RayCastWeapon costructor")


func _primary_use() -> bool:
	if ._primary_use():
		# Raycast from the barrel of the gun
		var origin = shoot_origin.global_transform.origin
		var shoot_dir:Vector3 = self.player.weapon_system.shoot_dir

		DrawLine3d.DrawRay(origin, shoot_dir * 1000, Color(0, 1, 0), 20)
		if stats["pierce"]:
			var result2 = RaycastUtils.raycast(origin, shoot_dir * stats["fire_distance"], [player])
			if not result2.empty():
				cause_damage(result2.collider)
		else:
			var result2 = RaycastUtils.raycast_pierce(origin, shoot_dir * stats["fire_distance"], [player])
			if not result2.empty():
				for hit in result2:
					cause_damage(hit.collider)
		return true
	return false


func cause_damage(body) -> void:
	if body.get("health"):
		if body.health.has_method("hit"):
			Debug.Print("bullets", "Colliding with player")
			body.health.hit(self.stats.damage)
