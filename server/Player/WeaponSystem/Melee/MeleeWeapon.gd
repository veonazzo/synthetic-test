extends ReloadWeapon
class_name MeleeWeapon


func init(player, type, owner_node, weapon_name, stats, extras=null) -> void:
	.init(player, type, owner_node, weapon_name, stats, extras)

	Debug.Print("weapons", "Test MeleeWeapon costructor")


func _primary_use() -> bool:
	if ._primary_use():		
		return true
	return false
