extends Node
class_name DamageSystem


export var max_health := 100.0
onready var _health: float = max_health


func set_health(new_health: float) -> void:
	_health = clamp(new_health, 0, max_health)


func get_health() -> float:
	return _health


func decrease_health(value: float) -> void:
	set_health(_health - value)


func increase_health(value: float) -> void:
	set_health(_health + value)


func _physics_process(_delta: float) -> void:
	set_health(_health)


func hit(damage: float) -> bool:
	Debug.Print("damage", "Hit method has been called")

	decrease_health(damage)

	if is_dead():
		return false

	return true

	
func is_dead() -> bool:
	return _health == 0
