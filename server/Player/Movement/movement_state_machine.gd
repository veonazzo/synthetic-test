extends StateMachine


export(NodePath) onready var movement = get_node(movement)
onready var player = parent

var _initialized := false


func _ready():
	add_state("walking")
	add_state("dashing")
	add_state("dodging")
	add_state("sliding")
	add_state("shifting")
	add_state("jumping")
	add_state("wall_jumping")
	add_state("falling")

	# Chiamo le funzioni durante l'idle, in modo da essere sicuro che il nodo padre
	# sia attivo.
	call_deferred("set_state", states.walking)
	call_deferred("_initialize")


func _initialize():
	movement = player.movement


func _state_logic(delta: float) -> void:
	match state:
		states.walking:
			movement.walk.act()

		states.jumping:
			if movement.dash.is_dashing():
				movement.dash.act()
			else:
				movement.walk.act()

		states.falling:
			movement.walk.act()

		states.dashing:
			movement.dash.act()

		states.dodging:
			movement.dodge.decelerate(delta)
			return

		states.sliding:
			movement.slide.act(delta)
			return

		states.shifting:
			movement.shift.move()

		states.wall_jumping:
			movement.wall_jump.move(delta)
			return

	_default_behavior(delta)


func _get_transition(_delta):
	match state:
		states.walking:
			if movement.dodge.has_to_dodge():
				return states.dodging
			if movement.jump.has_to_jump():
				return states.jumping
			if movement.dash.is_dashing():
				return states.dashing
			if player.input_system.is_pressed("shift"):
				return states.shifting
			if player.has_to_fall():
				return states.falling

		states.falling:
			if movement.dodge.has_to_dodge():
				return states.dodging
			if player.is_on_floor():
				return states.walking
			if movement.wall_jump.has_to_wall_jump() and previous_state != states.wall_jumping:
				return states.wall_jumping

		states.dashing:
			if movement.jump.has_to_jump():
				return states.jumping
			if player.has_to_fall() and movement.is_far_from_floor():
				return states.falling
			if not movement.dash.is_dashing():
				return states.walking
			if player.input_system.is_pressed("slide"):
				return states.sliding

		states.jumping:
			var jump_finished: bool = player._actual_velocity.y <= 0

			if jump_finished:
				return states.falling
			if movement.dodge.has_to_dodge():
				return states.dodging
			if movement.wall_jump.has_to_wall_jump():
				return states.wall_jumping

		states.sliding:
			var is_going_forward = player.input_system.is_pressed("forward") or player.input_system.is_pressed("dash")

			if movement.jump.has_to_jump():
				return states.jumping
			if not is_going_forward or not player.input_system.is_pressed("slide"):
				return states.walking
			if player.is_not_moving():
				return states.walking
			if player.has_to_fall() and movement.is_far_from_floor():
				return states.falling

		states.shifting:
			if movement.dodge.has_to_dodge():
				return states.dodging
			if movement.jump.has_to_jump():
				return states.jumping
			if not player.input_system.is_pressed("shift"):
				return states.walking
			if player.has_to_fall():
				return states.falling

		states.dodging:
			if movement.jump.has_to_jump():
				return states.jumping
			if player.is_not_moving():
				return states.walking
			if player.has_to_fall() and player.is_not_moving():
				return states.falling

		states.wall_jumping:
			if movement.wall_jump.has_to_land():
				return states.falling
			if player.is_on_floor():
				return states.walking
			if movement.dodge.has_to_dodge():
				return states.dodging


func _enter_state(new_state, _old_state) -> void:
	match new_state:
		states.jumping:
			movement.jump.jump()

		states.sliding:
			movement.shift.shift()
			if player._actual_velocity.y < 0:
				movement.slide.initial_sprint()

		states.wall_jumping:
			movement.wall_jump.act()

		states.dodging:
			movement.dodge.act()


func _exit_state(old_state, _new_state) -> void:
	match old_state:
		states.sliding:
			movement.slide.stop_sliding()
			movement.shift.stand_up()

		states.shifting:
			movement.shift.stand_up()

		states.dashing:
			player.input_system.action_release("dash")

		states.sliding:
			movement.shift.stand_up()


func _default_behavior(delta) -> void:
	if player.has_to_fall():
		player.apply_gravity(delta)

	player.move_horizontally(delta)
