extends Movement


export(float) var force := 30


func jump() -> void:
	player.add_force(Vector3.UP * force)


func has_to_jump() -> bool:
	return player.input_system.is_just_pressed("jump") and player.is_on_floor()
