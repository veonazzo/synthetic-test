extends Movement


export var dashing_speed := 50
export var dash_duration := 6 # in secondi.


func is_dashing() -> bool:
	return player.input_system.is_pressed("dash") and _has_enough_energy_to_dash()


func act():
	var energy_consumption = player.energy.max_energy / dash_duration * 0.1

	movement_system.set_moving_direction_with_speed(dashing_speed)
	player.energy.decrease(energy_consumption, "dash", 0.1)


func _physics_process(_delta):
	if not is_dashing() and player.input_system.is_pressed("dash"):
		player.input_system.action_release("dash")


func _has_enough_energy_to_dash() -> bool:
	var energy_consumption = player.energy.max_energy / dash_duration * 0.1
	return player.energy._energy >= energy_consumption
