extends Movement


export var acceleration: float


func apply(delta):
	player.move_horizontally_gradually(acceleration, delta)
