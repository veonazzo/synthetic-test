extends Node


var _double_press_actions: Dictionary
var _max_ms_per_input = 300


func register_double_press_action(action: String, action_to_simulate: String):
	_double_press_actions[action] = {
		"action_to_simulate": action_to_simulate, 
		"presses_count": 0, 
		"last_input_timestamp": 0
		}


func _physics_process(_delta):
	_check_for_pressed_keys()
	_simulate_actions()


func _check_for_pressed_keys():
	for action in _double_press_actions.keys():
		if Input.is_action_just_pressed(action):
			_increment_action_counter(action)


func _increment_action_counter(action: String) -> void:
	var action_data = _double_press_actions[action]
	var current_time: int = OS.get_ticks_msec()
	var last_input_timestamp: int = action_data["last_input_timestamp"]
	var presses_count: int = action_data["presses_count"]

	if current_time - last_input_timestamp > _max_ms_per_input:
		presses_count = 0
	
	_double_press_actions[action]["presses_count"] = clamp(presses_count + 1, 0, 2)
	_double_press_actions[action]["last_input_timestamp"] = current_time


func _simulate_actions():
	for action in _double_press_actions.keys():
		var double_pressed: int = _double_press_actions[action]["presses_count"] == 2
		var action_to_simulate: String = _double_press_actions[action]["action_to_simulate"]
		
		if double_pressed:
			Input.action_press(action_to_simulate)
			_double_press_actions[action]["presses_count"] = 0
