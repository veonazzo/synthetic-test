class_name StateMachine
extends Node

var state = null setget set_state
var previous_state = null
var states: Dictionary

onready var parent = get_parent()


func set_state(new_state) -> void:
	previous_state = state
	state = new_state
	
	if previous_state:
		_exit_state(previous_state, state)
	if new_state:
		_enter_state(state, previous_state)


func add_state(state_name) -> void:
	states[state_name] = state_name


func _physics_process(delta: float):
	if state != null:
		_state_logic(delta)
		
		var transition = _get_transition(delta)
		if transition:
			set_state(transition)


# Si controllano le azioni del nodo padre in base allo stato.
func _state_logic(delta: float) -> void:
	pass


# Ritorna lo stato in cui si deve passare.
func _get_transition(delta: float):
	return null


func _enter_state(new_state, old_state) -> void:
	pass
	

func _exit_state(old_state, new_state) -> void:
	pass
