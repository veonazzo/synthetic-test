class_name NetHandler
extends Node

const NETWORKED_PLAYER_SCENE: PackedScene = preload("res://Network/player/NetworkedPlayer.tscn")

var _network_manager#: NetworkManager
var _multiplayer_api: MultiplayerAPI
var _buffer: StreamPeerBuffer
var _connected_players: Node

func _ready():
	_network_manager = get_parent() #as NetworkManager
	_buffer = StreamPeerBuffer.new()
	_connected_players = get_node("/root/Server/World/Players")


func init(multiplayer_api: MultiplayerAPI):
	_multiplayer_api = multiplayer_api
	_connect_main_signals()


func _connect_main_signals():
	_multiplayer_api.connect("network_peer_connected", self, "_player_connected")
	_multiplayer_api.connect("network_peer_disconnected", self, "_player_disconnected")
	_multiplayer_api.connect("network_peer_packet", self, "_player_packet")
	print("Listening on port: ", _network_manager.SERVER_PORT)

func _player_connected(id: int):
	var new_player: Node = NETWORKED_PLAYER_SCENE.instance()
	new_player.name = str(id)
	_connected_players.add_child(new_player)
	
	print("utente di id %d si è connesso" % [id])
	
func _player_disconnected(id: int):
	var player: Node = _connected_players.get_node(str(id))
	_connected_players.remove_child(player)
	player.queue_free()
	
	print("utente con id %d si è disconnesso" % [id])

func _player_packet(id: int, packet_bytes: PoolByteArray):
	#print("l'utente %d ha inviato un pacchetto" % [id])
	_buffer.seek(0)
	_buffer.data_array = packet_bytes
	
	var packet: Packet = Packet.get_root_as(_buffer, Packet.new())
	var packet_content_type = packet.packet_content_type()
	if (packet != null && packet_content_type == PacketContent.ClientContent):
		var client_packet: ClientContent = packet.packet_content(ClientContent.new())
		if client_packet != null:
			var client_packet_type = client_packet.content_type()
			if client_packet_type >= 0:
				match client_packet_type:
					ClientPacketContent.CPacketConnectionInit:
						_connection_init_handler(
							id, 
							client_packet.content(CPacketConnectionInit.new())
						)
					ClientPacketContent.CPacketInput:
						_packet_input_handler(
							id, 
							client_packet.content(CPacketInput.new())
						)
					ClientPacketContent.CPacketActions:
						_packet_actions_handler(
							id, 
							client_packet.content(CPacketActions.new())
						)
					ClientPacketContent.CPacketWeaponRotation:
						_packet_weapon_rotation_handler(
							id, 
							client_packet.content(CPacketWeaponRotation.new())
						)
					_:
						printerr("Ricevuto pacchetto con id non valido")


func _connection_init_handler(id: int, packet: CPacketConnectionInit) -> void:
	var player: NetworkedPlayer = _connected_players.get_node(str(id))
	if player:
		if player.connection_init(packet.access_token()) == ConnectionState.CONNECTED:
			#TODO remove this hardcoded message
			_network_manager.send_spacketconnnectionaccepted(id, "Benvenuto nel server")
			
			var rotation_degrees: Vector3 = player.get_rotation_degrees()
			rotation_degrees.x = _degree_to_short(rotation_degrees.x)
			rotation_degrees.y = _degree_to_short(rotation_degrees.y)
			rotation_degrees.z = _degree_to_short(rotation_degrees.z)
			_network_manager.send_spacketplayerjoin(
				-id, #send to everyone except the player that connected
				player.username,
				player.display_name,
				player.nakama_user_id,
				rotation_degrees,
				player.get_translation()
			)
			for connected_player in _connected_players.get_children():
				if connected_player.name != str(id):
					rotation_degrees = player.get_rotation_degrees()
					rotation_degrees.x = _degree_to_short(rotation_degrees.x)
					rotation_degrees.y = _degree_to_short(rotation_degrees.y)
					rotation_degrees.z = _degree_to_short(rotation_degrees.z)
					_network_manager.send_spacketplayerjoin(
						id,
						connected_player.username,
						connected_player.display_name,
						connected_player.nakama_user_id,
						rotation_degrees,
						connected_player.get_translation()
					)
		else:
			_network_manager.send_spacketdisconnect(id, "Session expired or invalid token")
			get_tree().get_network_peer().disconnect_peer(id)
	
	print("Ricevuto connection init packet")
	
func _packet_weapon_rotation_handler(id: int, packet: CPacketWeaponRotation) -> void:
	if !packet:
		return
	
	var rotation_degrees = packet.angles()
	if rotation_degrees != null:
		
		rotation_degrees = Vector3(
			_short_to_normal_vector(rotation_degrees.yaw()),
			_short_to_normal_vector(rotation_degrees.pitch()),
			_short_to_normal_vector(rotation_degrees.roll())
		)
		_connected_players.get_node(str(id)).weapon_system.shoot_dir = rotation_degrees
	
func _short_to_normal_vector(short_val: int) -> float:
	return (short_val * (2.0 / 65536))
	
func _packet_actions_handler(id: int, packet_actions: CPacketActions) -> void:
	if packet_actions.input_actions_length() == 0:
		return
	
	_connected_players.get_node(str(id)).input_system.process_actions(packet_actions)


func _packet_input_handler(id: int, packet_input: CPacketInput) -> void:
	if !packet_input:
		return
	
	var rotation_degrees = packet_input.angles()
	if rotation_degrees != null:
		rotation_degrees = Vector3(
			_short_to_degrees(rotation_degrees.yaw()),
			_short_to_degrees(rotation_degrees.pitch()),
			_short_to_degrees(rotation_degrees.roll())
		)
		_connected_players.get_node(str(id)).set_rotation_degrees(rotation_degrees)

	_connected_players.get_node(str(id)).weapon_system.current = packet_input.weapon() 
	_connected_players.get_node(str(id)).weapon_system._change_weapon()

	#_connected_players.get_node(str(id)).inputs.push_back(packet_input)

func _short_to_degrees(short_val: int) -> float:
	return (short_val * (360.0 / 65536))


#Temp function, move away
func _degree_to_short(degree: int) -> int:
	return (int((degree)*65536/360) & 65535)
