extends "res://Player/player.gd"
class_name NetworkedPlayer

var _connection_state: int
var username: String
var display_name: String
var nakama_user_id: UUIDClass

var inputs: Array

#TEMP var move away
var rng = RandomNumberGenerator.new()

func _enter_tree():
	rng.randomize()
	_connection_state = ConnectionState.NEW
	inputs = []


func connection_init(access_token: String) -> int:
	_connection_state = ConnectionState.CONNECTING
	
	#Sostituire con verifica del JWT
	var is_access_token_valid: bool = true 
	if is_access_token_valid:
		_connection_state = ConnectionState.CONNECTED

		# TEST CODE TO BE REMOVED
		username = "JudGenie"
		display_name = access_token #temp value
		nakama_user_id = UUIDClass.new("bedd7605-63a3-4f18-bbcd-4c7bc79a7a70")
		nakama_user_id.most_sig_bits = rng.randi()
	else:
		_connection_state = ConnectionState.FAILED
	
	return _connection_state

func _physics_process(delta):
	if inputs.size() > 0:
		for i in range(inputs.size()):
			var packet_input: CPacketInput = inputs.pop_front()
			while (packet_input != null):
				_process_input_packet(packet_input)
				packet_input = inputs.pop_front()


func _process_input_packet(input_packet: CPacketInput) -> void:
	if !input_packet:
		return
		
	var rotation_degrees = input_packet.angles()
	if rotation_degrees != null:
		rotation_degrees = Vector3(
			_short_to_degrees(rotation_degrees.yaw()),
			_short_to_degrees(rotation_degrees.pitch()),
			_short_to_degrees(rotation_degrees.roll())
		)
		print(rotation_degrees)
		self.set_rotation_degrees(rotation_degrees)

	
func _short_to_degrees(short_val: int) -> float:
	return (short_val * (360.0 / 65536))
