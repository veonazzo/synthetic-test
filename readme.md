# Update

Godot 3.2.2 and the Mono version should work. See the Perquisites section for
how to use it.

GitHup Actions support added. See [#GitHub](#GitHub) for details on how to use
it.

# Build Automation for Godot in GitLab (and GitHub!)

This project started as tutorial for build automation. While working out how to
explain build automation for Godot, I realized most of the steps could be
completely automated for most Godot Projects. The project quickly evolved into a
complete build automation solution for Godot. As of writing this, I have used
this exact project (occasionally with minor tweaks) to build virtual Godot
project I have done.

If you are interested in learning how to build your own build automation system,
I believe `./builder` is *fairly* readable. Most of this script is geared toward
prepping Godot command line arguments and collecting artifacts.

This project is an experiment. I have created a docker container and a large
build script that should compile all Godot Headless compatible projects, however
I make no guarantees. If you find bugs in the system, **please** let me know
about your project and I will look into it.

[Link to example output!](https://greenfox.gitlab.io/godot-build-automation/)


# Prerequisites

## A Project That Is Compatible

I believe most project should be compatible, however there are some known issues
with certain types of projects:

- Untested:
  - GDNative: You will need write steps for automating compiling your native
    modules. I believe then it's only a matter of building the resulting
    project. 
  - OSX: I have heard reports this works, but I haven't tested it.
  - Windows Universal: Completely untested. Anyone want to take this on?
  - Android: I don't see why this *wouldn't* work, but will probably require a special build of the container. 
  - iOS:  I do not have the ability to test this.
- Tested:
  - C#: You have use the "right" docker container for C#, find the image line of your CI/CD file (different for GitLab and GitHub) and change the last part to `:mono`, for example: 
    - `image: registry.gitlab.com/greenfox/godot-build-automation:mono`

For most people, a pure GDScript Project targeting Linux, Windows, and HTML5
should work perfectly.

## GitHub or GitLab

This was originally designed for GitLab CI, but I have added GitHub Actions.
GitLab CI is the intended system and will be supported as a "first class"
platform.

Teaching Git is beyond the scope of this project. I'm going to assume your
project is already in a Git repo locally.

### GitLab

If you're using Git, you're probably using GitHub. When this project was
created, GitHub Actions didn't exist and GitLab was the best all-in-one Git+CI
solution. I still recommend GitLab.

Adding your project to GitLab is very similar to adding it to GitHub. Create a
new repository or use an existing repository and add follow the instructions.

GitLab's free tier gives you 2,000 CI minutes per month so unless your project
takes more than 33 hours out of every month to build, you're probably not going
to run into issues. If you do, consider building only on certain branches,
buying Silver or Gold tier, or starting your own GitLab Runner (this is what I
do).

Once you're project is on GitLab, you will need to add `./.gitlab-ci.yml` to the
root directory of the project. If you would like to rename this file or move it
to another directory, go to your repo, see `Settings` -> 
`CI/CD->General pipelines` -> `Custom CI configuration path`.

### GitHub

If you would really like to continue using GitHub for you repo, you can either
mirror your repo to GitLab (allowing GitLab CI to build changes) or use the new
GitLab Actions script. I don't daily drive GitHub Actions the way I do GitLab
CI, so if I break something, please let me know!

To use GitHub Actions, add `./.github/workflows/BuildGodot.yml` to your project.
You can rename this file to anything you want, but it must be in `./.github/workflows/`.

If you want to use GitHub Pages, you will have to turn that on in `Settings` ->
`GitHub Pages` and select branch `gh-pages`. If you do not want to use GitHub
Pages, you'll have to publish builds another way, see
[Artifacts](https://docs.github.com/en/actions/configuring-and-managing-workflows/persisting-workflow-data-using-artifacts)
or use something like [Itch.io's Butler](https://itch.io/docs/butler/).

[I have an example implementation here.](https://github.com/greenfox1505/Godot-Build-Automation)

## No `./public` Directory

`./public` is off limits for this build system to work as intended. If you have
a folder called `./public` in the root of your project, it will cause the build
to fail. `./public` is absolutely needed for GitLab Pages to work. For
compatibility reasons, this project handles GitHub Pages with the same workflow.

If you don't care about GitLab pages, there is a way to redirect your build
target and it is documented in `./builder -h`. For simplicity, I won't go over
that example unless it is requested.

## `export_presets.cfg`

By default, most projects will likely have `export_presets.cfg` in your
`.gitignore` file, leaving it out of your repository. This is recommended by
[godotengine/godot-demo-projects/.gitignore](https://github.com/godotengine/godot-demo-projects/blob/master/.gitignore).
I believe the reasoning behind this has to do with private keys used for some
build targets (like mobile). (Update: it looks like there might be some changes
on this front in 4.0, but we'll see)

For this build automation system to work, you need to include an
`export_presets.cfg` file in your projects. There may be a way to generate a bog
standard `export_presets.cfg` (with most build targets configured) if the
builder script does not find one. I have not implemented that yet, so for the
forseeable future, `export_presets.cfg` is still required.

## `./project.godot`

Every Godot Project has a `project.godot` file. However, for this script to
work, that file must be in the root directory of your project, ie:
`./project.godot`. 

If your `project.godot` is in some other directory, you'll have to add some
flags to `builder` line of `.gitlab-ci.yml`(for GitLab) or `BuildGodot.yml` (for
GitHub) and add `-p ${PROJECT_PATH}/project.godot` (replacing PROJECT_PATH with
the path to your project). In this demo project, [I do that on line 22 of this
example.](https://gitlab.com/greenfox/godot-build-automation/-/blob/098035311a810f219857f262f3bed0272ba68fd0/DockerContainerBuilder/build_containers.yml#L22)

## Working Build Targets

Currently, I have the tools at my disposal to test the following build targets:
- Linux/X11
- Windows Desktop
- HTML5

I have included, but have not tested, these build targets:
- Mac OSX
- Windows Universal

Other platforms (such as Android and iOS) are outside of my ability to test. I
am willing to try to expand this build system to those targets if is possible to
do so, but I will need help to that.

# How To Make This Work

- Add your project to GitLab.com
- Add the the appropriate CI config from this repo your repo.
  - For GitLab: `./.gitlab-ci.yml`
  - For GitHub: `./.github/workflows/BuildGodot.yml`
- Have coffee

These steps are not numbered because they can be done in any order. 

**Yes, it really is that simple!**
----------------------------------

Once you've committed your project to GitLab and added your CI config file, it
should just work. 

For GitLab there is a banner on your GitLab project page listing the last
commit. A small circle well tell you the status of the build. Click that circle
to see the build pipeline. Yellow for Pending (gotta wait your turn unless you
pay for GitLab Bronze or better), Blue for Actively Building, Red for failure
[(see bottom)](#help-it-broke), and Green for success.

Once it's done, your project will be found on the web at
`https://${Your_GitLab_Name}.gitlab.io/${Your_Project_Name}` (from your project
page, click settings, pages, and there should be a direct link). This can be
disabled if you chose, but isn't by default. The first time your project builds,
this will not come up right away. It may take 15-60 minutes to create your
project page for the first time. However, after it has been created, it will
update almost as soon as your build completes.

For GitHub, click that "Actions" button at the top of the page. Once it's done,
it should show up at: `https://${Your_GitHub_Name}.github.io/${Your_Project_Name}/`


# How does it work?

(this explication was written about GitLab CI. GitHub Actions works similarly)

Every time you run a commit, GitLab will look into your project, read the
`.gitlab-ci.yml` run a build.

It starts with a docker image,
`registry.gitlab.com/greenfox/godot-build-automation`. This image is an Ubuntu
image with some extra packages installed as well as Godot. It extracts Godot
Headless to `/usr/local/bin/godot` and build templates to
`~/.local/share/godot/templates/`. If your not familar with docker, it's kind of
somewhere between a Virtual Machine and a Bare Metal application. It's has
virtualized networking and disk space, but it's still sharing the same kernel as
your base OS. If that doesn't make sense, don't worry, GitLab will handle it.

If you wish to design your own build automation system with the Headless version
of Godot, it's very obvious how to download Godot and run the binary form
wherever you've placed it. However, it is not obvious where the build templates
go. For `3.1.1`, the build templates are expected to be in
`~/.local/share/godot/templates/3.1.1.stable/` (for example, the path of
`windows_32_debug.exe` should be
`~/.local/share/godot/templates/3.1.1.stable/windows_32_debug.exe`)

Once the docker image is pulled, `builder` is called. This command is the script
in this repo called `./builder`. As a GitLab build, it will search for your
`project.godot` file in the current directory for your project's properties and
search `export_presets.cfg` to find your build targets. It will used the
information gathered there to build every export target you have defined.

Assuming Godot in installed correctly, the project is configured, and
${TARGET_DIR} exists, this one line does all the real work:<br> `godot -v
"${PROJECT_FILE}" --export "${EXPORT_NAME}"
"${TARGET_DIR}/${PROJECT_NAME}${FILE_ENDING}"`


Lastly, the script will zip up everything. This zipping step will be skipped for
any build target called `HTML5`, so that they can be played in a web browser
from GitLab.

# What Else Can I Do?

Right now this system will only build projects and will only build every build
target you have configured within Godot. You can use this build script to modify
how it builds, the project name, and a few other parameters. To do that, I
expect you have a working knowledge of Linux and Bash and can download
`./builder` and run it locally.

If you're willing to learn a bit about the `.gitlab-ci.yml` format, you can
trigger builds conditionally. For example only build when you merge back to
"master". Or only build when you have "BUILD ME" or some other text string in
your commit message. You can also configure a "conditional" build that requires
you to manually press a button; this is useful for choosing what builds to
publish to distribution.

# Help, It Broke

COOL!, I absolutely want to hear about every edge case that could break my
script. I've found a few projects, but certainly not everything. If your use
case is legitimate and you're willing to share with me a repo that can trigger
this issue then I will try to take the time to fix that.

# Known Issues

## C#

This should be fixed. Please let me know if it works!

# Advanced Topics

See [Advanced Topics](advanced_topics.md)

# `build.sh`/`builder` Release Notes
- 1.4
  - added GitHub Actions integration. There is defiantly a cleaner way to do
    this, but I can't be bothered yet. 
- 1.3
  - added -v to give you the version number
  - added Godot 3.2, see the new advanced section to change what your version is
- 1.2
  - Migrating docker container from Docker Hub to Gitlab
  - Adding scripts to compile Godot Engine itself (see
    [GodotCompiler/README.md](./GodotCompiler/README.md))
