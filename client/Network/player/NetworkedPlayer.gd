extends Spatial
class_name NetworkedPlayer

var _nametag_label

var _connection_state: int
var username: String
var display_name: String
var user_id: UUIDClass

func _enter_tree():
	_connection_state = ConnectionState.CONNECTED
	_nametag_label = get_node("NameTag/Viewport/NametagLabel")

	_nametag_label.text = display_name
	
