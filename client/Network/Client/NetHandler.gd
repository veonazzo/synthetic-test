class_name NetHandler
extends Node

#Move away
const PROTOCOL_VERSION = 123
const NETWORKED_PLAYER_SCENE: PackedScene = preload("res://Network/player/NetworkedPlayer.tscn")

export (NodePath) onready var _local_player = get_node(_local_player)

var _network_manager#: NetworkManager
var _multiplayer_api: MultiplayerAPI
var _buffer: StreamPeerBuffer
var _networked_players: Node

func _ready():
	_network_manager = get_parent() #as NetworkManager
	_buffer = StreamPeerBuffer.new()
	_networked_players = get_node("/root/Client/World/NetworkedPlayers")


func init(multiplayer_api: MultiplayerAPI):
	_multiplayer_api = multiplayer_api
	_connect_main_signals()


func _connect_main_signals():
	_multiplayer_api.connect("connected_to_server", self, "_connected_to_server")
	_multiplayer_api.connect("server_disconnected", self, "_player_disconnected")
	_multiplayer_api.connect("network_peer_packet", self, "_on_packet")


func _connected_to_server():
	var nakama_session: NakamaSession = get_node("/root/Client/Networking/Nakama").session
	if nakama_session:
		var nakama_token = get_node("/root/Client/Control/NakamaLogin/AccessToken")
		_network_manager.send_cpacketconnectioninit(PROTOCOL_VERSION, nakama_token.text)
		print("Connesso correttamente al server")
	else:
		_network_manager.close_connection()


func _player_disconnected():
	_network_manager.connection_state = ConnectionState.NEW
	print("Disconnesso dal server")

# Packet handling methods

func _on_packet(id: int, packet_bytes: PoolByteArray):
	#print("Ricevuto un pacchetto da %d" % [id])
	_buffer.seek(0)
	_buffer.data_array = packet_bytes

	#_print_stream_buffer(_buffer)
	var packet: Packet = Packet.get_root_as(_buffer, Packet.new())
	var packet_content_type = packet.packet_content_type()
	if (packet != null && packet_content_type == PacketContent.ServerContent):
		var server_packet: ServerContent = packet.packet_content(ServerContent.new())
		var server_packet_type = server_packet.content_type()
		if (server_packet != null && server_packet_type >= 0):
			match server_packet_type:
				ServerPacketContent.SPacketConnectionAccepted:
					_connection_accepted_handler(
						server_packet.content(SPacketConnectionAccepted.new())
					)
				ServerPacketContent.SPacketDisconnect:
					_disconnect_handler(
						server_packet.content(SPacketDisconnect.new())
					)
				ServerPacketContent.SPacketPlayerJoin:
					_player_join_handler(
						server_packet.content(SPacketPlayerJoin.new())
					)
				ServerPacketContent.SPacketPlayerQuit:
					_player_quit_handler(
						server_packet.content(SPacketPlayerQuit.new())
					)
				ServerPacketContent.SPacketGameState:
					_gamestate_handler(
						server_packet.content(SPacketGameState.new())
					)
				_:
					printerr("Ricevuto pacchetto con id non valido")

func _connection_accepted_handler(packet: SPacketConnectionAccepted):
	if not packet:
		return
	_network_manager.connection_state = ConnectionState.CONNECTED
	print("Ricevuto connection accepted packet Hello MSG: %s" % [packet.hello_message()])


func _disconnect_handler(packet: SPacketDisconnect):
	if not packet:
		return

	print("Ricevuto pacchetto di disconnessione Motivazione %s" % packet.message())
	_network_manager.close_connection()


func _player_join_handler(packet: SPacketPlayerJoin):
	if not packet:
		return

	var new_player: Node = NETWORKED_PLAYER_SCENE.instance()
	var uuid: UUID = packet.user_id()
	new_player.user_id = UUIDClass.new().from_most_least_sig_bits(
		uuid.high_bytes(), uuid.low_bytes()
	)
	new_player.name = str(new_player.user_id)
	new_player.username = packet.username()
	new_player.display_name = packet.display_name()

	var rotation_degrees = packet.rotation()
	if rotation_degrees:
		rotation_degrees = Vector3(
			_short_to_degrees(rotation_degrees.yaw()),
			_short_to_degrees(rotation_degrees.pitch()),
			_short_to_degrees(rotation_degrees.roll())
		)
		new_player.set_rotation_degrees(rotation_degrees)

	var position = packet.position()
	if position:
		position = Vector3(
			position.x(),
			position.y(),
			position.z()
		)
		new_player.set_translation(position)

	_networked_players.add_child(new_player)


func _player_quit_handler(packet: SPacketPlayerQuit):
	if not packet:
		return

	var uuid = packet.user_id()
	uuid = UUIDClass.new().from_most_least_sig_bits(uuid.high_bytes(), uuid.low_bytes())
	var player: Node = _networked_players.get_node(str(uuid))
	_networked_players.remove_child(player)
	player.queue_free()


func _gamestate_handler(packet: SPacketGameState):
	if not packet:
		return

	if not packet.player_states_is_none():
		var player_states_length: int = packet.player_states_length()
		if player_states_length > 0:
			for i in range(player_states_length):
				var player_state: PlayerGamestate = packet.player_states(i)
				_player_gamestate_handler(player_state)


func _player_gamestate_handler(player_gamestate: PlayerGamestate):
	if not player_gamestate:
		return

	var uuid = player_gamestate.uuid()
	if uuid:
		uuid = UUIDClass.new().from_most_least_sig_bits(
			uuid.high_bytes(),
			uuid.low_bytes()
		)
		var connected_player: Spatial = _networked_players.get_node_or_null(str(uuid))
		if not connected_player:
			connected_player = _local_player
			var health = player_gamestate.health()
			if health:
				connected_player.health.set_health(health)

		var position = player_gamestate.position()
		if position:
			position = Vector3(
				position.x(),
				position.y(),
				position.z()
			)
			connected_player.set_translation(position)
		
		

###############################################################################

func _print_stream_buffer(stream_buffer: StreamPeerBuffer):
	var available_bytes: int = stream_buffer.get_available_bytes()
	print("Size: %d, available: %d" % [stream_buffer.get_size(), available_bytes])
	var output: String = "["
	for i in range(0, available_bytes - 1):
		output += str(stream_buffer.get_8()) + ", "
	print("%s%d]" % [output, stream_buffer.get_8()])
	stream_buffer.seek(stream_buffer.get_size() - available_bytes)


func _short_to_degrees(short_val: int) -> float:
	return (short_val * (360.0 / 65536))
