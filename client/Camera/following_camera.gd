extends Position3D


export(NodePath) onready var target = get_node(target)
export(Vector3) var anchor_offset: Vector3
export(Vector2) var pitch_limit: Vector2 # Limite rotazione sulla x in gradi.

export var following_speed := 20

export var spring_arm_size := Vector2(1.1, 0.6) # Dimensioni del parallelepipedo.
# Di quanto spostare la camera dal punto di collisione del braccio a molla.
export var spring_arm_margin := 3.5
# La velocità con la quale la camera si muove verso/lontano dal giocatore quando la vista è ostruita.
export var zoom_speed := 7
# La dimensione della griglia di raycast che simulano il parallelepipedo del braccio a molla.
export var collision_accuracy := 3

export var hide_target_distance: float

var camera_offset: Vector3

# Per sapere sempre dove si dovrebbe trovare la camera e come dovrebbe essere ruotata uso un segnaposto.
onready var _camera_placeholder := $CameraPlaceholder
onready var _camera := $Camera

var _cam_velocity: Vector3 = Vector3.ZERO
var _last_pos: Vector3 = global_transform.origin


func _ready():
	global_transform.origin = target.global_transform.origin
	_setup_camera_pos()
	_camera_placeholder.look_at(target.global_transform.origin, Vector3.UP)


func _setup_camera_pos() -> void:
	var cam = _camera_placeholder

	camera_offset = cam.global_transform.origin - global_transform.origin
	cam.global_transform.origin = global_transform.origin + camera_offset

	_camera_placeholder = cam
	_camera.global_transform = cam.global_transform


func _physics_process(delta):
	_follow_target(delta)
	_move_past_obstacles(delta)
	_calculate_cam_velocity()


func _unhandled_input(event):
	if event is InputEventMouseMotion:
		var x_delta = event.relative.y * GlobalSettings.mouse_sensitivity
		rotation_degrees.x += x_delta

		rotation_degrees.x = clamp(rotation_degrees.x, pitch_limit.x, pitch_limit.y)


func _move_past_obstacles(delta) -> void:
	var obstacle_distance := _check_obstacle_distance()
	
	var zoom_faster_when_high_velocity = clamp(_cam_velocity.length() * 1.2, 1, 3)
	var zoom_in_step = clamp(zoom_speed * zoom_faster_when_high_velocity * delta, 0.1, 1)
	var zoom_out_step = clamp(zoom_speed * delta, 0.1, 1)
	
	var new_pos: Vector3 = target.global_transform.origin
	var cam_pos = _camera_placeholder.global_transform.origin
	var target_transform = target.global_transform
	var target_facing_cam = target_transform.looking_at(cam_pos, Vector3.UP)

	Debug.print_line("camera", "zoom_faster_when_high_velocity: " + str(zoom_faster_when_high_velocity))
	Debug.Print("camera", "zoom_in_step: " + str(zoom_in_step))
	
	new_pos -= target_facing_cam.basis.z * (obstacle_distance - spring_arm_margin)

	if obstacle_distance == 0: # No obstacles.
		_interpolate_camera_to(cam_pos, zoom_out_step)
	else:
		_interpolate_camera_to(new_pos, zoom_in_step)
		
	_modify_target_visibility()


func _interpolate_camera_to(new_pos: Vector3, step: float):
	var cam_pos: Vector3 = _camera.global_transform.origin

	cam_pos = cam_pos.linear_interpolate(new_pos, step)
	_camera.global_transform.origin = cam_pos
	_camera.rotation = _camera_placeholder.rotation


func _check_obstacle_distance() -> float:
	var target_transform: Transform = target.global_transform
	var cam_transform: Transform = _camera_placeholder.global_transform
	var result: Dictionary = RaycastUtils.grid_raycast(target_transform, cam_transform, collision_accuracy, spring_arm_size, [target])
	
	if result.has("position"):
		Debug.Print("camera", "Obstacle distance: " + str(global_transform.origin.distance_to(result["position"])))
		return global_transform.origin.distance_to(result["position"])
	else:
		return 0.0


func _follow_target(delta) -> void:
	var target_pos: Vector3 = target.global_transform.origin
	var follow_delta := clamp(following_speed * delta, 0.2, 1)
	var offset: Vector3 = VectorUtils.make_vector_relative_to(anchor_offset, target.global_transform)

	global_transform.origin = global_transform.origin.linear_interpolate(target_pos + offset, follow_delta)
	rotation.y = target.rotation.y


func _calculate_cam_velocity() -> void:
	_cam_velocity = _camera_placeholder.global_transform.origin - _last_pos
	_last_pos = _camera_placeholder.global_transform.origin
	Debug.print_line("camera", "_cam_velocity: " + str(_cam_velocity))


func _modify_target_visibility() -> void:
	var target_pos: Vector3 = target.global_transform.origin
	
	if target_pos.distance_to(_camera.global_transform.origin) < hide_target_distance:
		target.visible = false
	else: 
		target.visible = true
