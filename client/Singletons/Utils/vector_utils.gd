extends Node


func make_vector_relative_to(vector: Vector3, transform: Transform) -> Vector3:
	var relative_vector: Vector3 = Vector3.ZERO
	
	relative_vector += transform.basis.x * vector.x
	relative_vector += transform.basis.y * vector.y
	relative_vector += transform.basis.z * vector.z
	
	return relative_vector
