extends Weapon
class_name ReloadWeapon


var is_reloading : bool = false


# Constructor
func init(player, type, owner_node, camera, weapon_name, stats, crosshair, id, extras=null) -> void:
	.init(player, type, owner_node, camera, weapon_name, stats, crosshair, id, extras)

	Debug.Print("weapons", "Test ReloadWeapon costructor")


func _primary_use() -> bool:
	if stats["bullets"] > 0:
		if ._primary_use():
			stats["bullets"] -= 1
			return true
		return false
	return false


func _is_reloading() -> bool:
	var current_time: int = OS.get_ticks_msec()
	if self.prev_time == 0:
		self.prev_time = current_time
		return false
	if self.is_reloading == true:
		if (current_time - self.prev_time) > (self.stats["reload_time"] * 100):
			self.is_reloading = false
			return false
		return true
	return false


func _can_reload() -> bool:
	if not ._can_primary_use() or _is_reloading():
		return false
	return true


func _can_primary_use() -> bool:
	if not ._can_primary_use() or _is_reloading():
		return false
	return true


func _can_change() -> bool:
	if not ._can_change() or _is_reloading():
		return false
	return true


func _reload() -> void:
	if _can_reload():
		if stats["bullets"] < stats["max_bullets"] and stats["ammo"] > 0:
			Debug.Print("weapons", "Reloading")
			self.is_reloading = true
			var current_time: int = OS.get_ticks_msec()
			self.prev_time = current_time

			for b in stats["ammo"]:
				stats["bullets"] += 1
				stats["ammo"] -= 1

				if stats["bullets"] >= stats["max_bullets"]:
					Debug.Print("weapons", "Ammo: " + str(stats["ammo"]))
					break;
