extends Spatial
class_name Weapon


var player : KinematicBody
var type : int
var owner_node : Node #NOT the player
var camera : Camera
var weapon_name : String
var stats : Dictionary
var crosshair : TextureRect
var extras = null
var id : int

var prev_time : int = 0
var is_primary_using : bool = false
var active : bool = false

onready var mesh = owner_node.get_node("{}".format([name], "{}"))
onready var anim = $mesh/anim
onready var shoot_origin = $ShootOrigin

# Constructor
func init(player, type, owner_node, camera, weapon_name, stats, crosshair, id, extras=null) -> void:
	self.player = player
	self.type = type
	self.owner_node = owner_node
	self.camera = camera
	self.weapon_name = weapon_name
	self.stats = stats
	self.crosshair = crosshair
	self.extras = extras
	self.id = id

# Show the weapon
func _draw() -> void:
	# Check is visible
	if not mesh.visible:
		# Play draw animaton
		anim.play("Draw")
	active = true


# Hide the weapon
func _hide() -> void:
	# Check is visible
	if mesh.visible:
		# Play hide animaton
		anim.play("Hide" )
	active = false


func _primary_use() -> bool:
	if _can_primary_use():
		Debug.Print("weapons", "Primary Use")

		self.is_primary_using = true
		var current_time: int = OS.get_ticks_msec()
		self.prev_time = current_time

		return true
	return false


func _can_primary_use() -> bool:
	if _is_primary_using() or not self.active:
		return false
	else:
		return true


func _is_primary_using() -> bool:
	var current_time: int = OS.get_ticks_msec()
	if self.prev_time == 0:
		self.prev_time = current_time
		return false
	if self.is_primary_using == true:
		if (current_time - self.prev_time) > (100/self.stats["use_rate"]):
			self.is_primary_using = false
			return false
		else:
			return true
	else:
		return false


func _can_change() -> bool:
	if _is_primary_using():
		return false
	else:
		return true
