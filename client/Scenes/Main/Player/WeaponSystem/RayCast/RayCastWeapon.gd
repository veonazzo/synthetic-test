extends ReloadWeapon
class_name RayCastWeapon


# Constructor
func init(player, type, owner_node, camera, weapon_name, stats, crosshair, id, extras=null) -> void:
	.init(player, type, owner_node, camera, weapon_name, stats, crosshair, id, extras)

	Debug.Print("weapons", "Test RayCastWeapon costructor")


func _primary_use() -> bool:
	if ._primary_use():
		# Raycast from the barrel of the gun
		var origin = shoot_origin.global_transform.origin
		var ch_pos = crosshair.rect_position + crosshair.rect_size * 0.5

		var ray_from_camera = camera.project_ray_origin(ch_pos)
		var ray_dir_camera = camera.project_ray_normal(ch_pos)
		DrawLine3d.DrawRay(ray_from_camera, ray_from_camera + ray_dir_camera * 1000, Color(0, 0, 1), 20)
		var shoot_target
		var result = RaycastUtils.raycast(ray_from_camera, ray_from_camera + ray_dir_camera * 1000, [player])
		if result.empty():
			shoot_target = ray_from_camera + ray_dir_camera * 1000
		else:
			shoot_target = result.position
		var shoot_dir = (shoot_target - origin).normalized()
		print(shoot_dir)
		self.player.network_manager.send_cpacketweaponrotation(shoot_dir)

		DrawLine3d.DrawRay(origin, shoot_dir * 1000, Color(0, 1, 0), 20)
		if stats["pierce"]:
			var result2 = RaycastUtils.raycast(origin, shoot_dir * stats["fire_distance"], [player])
			if not result2.empty():
				cause_damage(result2.collider)
		else:
			var result2 = RaycastUtils.raycast_pierce(origin, shoot_dir * stats["fire_distance"], [player])
			if not result2.empty():
				for hit in result2:
					cause_damage(hit.collider)
		return true
	return false


func cause_damage(body) -> void:
	if body.get("health"):
		if body.health.has_method("hit"):
			Debug.Print("bullets", "Colliding with player")
			body.health.hit()
