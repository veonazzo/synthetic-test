class_name NoGravityProjectile
extends Projectile

var g = Vector3.ZERO # Gravity


# Constructor
func init(player:KinematicBody, projectile_name:String, stats:Dictionary) -> void:
	.init(player, projectile_name, stats)
	Debug.Print("bullets", "Test NoGravityProjectile costructor")


# Process physics
func _physics_process(delta) -> bool:
	if ._physics_process(delta):
		# Process Gravity
		velocity += g * delta
		# Turn the bullet in the right direction
		look_at(transform.origin + velocity.normalized(), Vector3.UP)
		transform.origin += velocity * delta
		return true
	return false


# Process collision
func _on_body_entered(body) -> bool:
	if ._on_body_entered(body):
		return true
	return false
