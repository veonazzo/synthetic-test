extends GravityProjectile
class_name GrenadeBullet


# Constructor
func init(player:KinematicBody, projectile_name:String, stats:Dictionary) -> void:
	.init(player, projectile_name, stats)
	Debug.Print("bullets", "Test GrenadeBullet costructor")


# Process physics
func _physics_process(delta) -> bool:
	if ._physics_process(delta):
		return true
	return false


# Process collision
func _on_body_entered(body) -> bool:
	if ._on_body_entered(body):
		return true
	return false
