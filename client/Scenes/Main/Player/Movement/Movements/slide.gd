extends Movement


export var deleceration := 1.1
export var slope_sliding_speed := 50
export var initial_sprint_force := 20


func act(delta: float) -> void:
	var floor_normal = player.get_floor_normal()
	var slope_sensitivity = deg2rad(10)
	var is_on_slope: bool = floor_normal.angle_to(Vector3.UP) > slope_sensitivity
	
	if player.has_to_fall():
		player.apply_gravity(delta)
	elif is_on_slope and player._actual_velocity.y < 0:
		movement_system.set_moving_direction_with_speed(slope_sliding_speed)
	else:
		movement_system.set_moving_direction_with_speed(0)
	
	player.move_horizontally(delta, deleceration)


func stop_sliding():
	player.input_system.action_release("slide")


func initial_sprint():
	player.add_force(player.global_transform.basis.z * initial_sprint_force)
