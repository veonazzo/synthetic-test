extends Movement


func _unhandled_input(event):
	if event is InputEventMouseMotion:
		var y_delta = -event.relative.x * GlobalSettings.mouse_sensitivity
		player.rotation_degrees.y += y_delta

