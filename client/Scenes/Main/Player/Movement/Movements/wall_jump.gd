extends Movement


export(float) var force
export(float) var jump_angle
export var fall_acceleration: float


func act() -> void:
	var player_basis: Basis = player.global_transform.basis
	var force_dir: Vector3 = -player_basis.z

	force_dir = force_dir.rotated(player_basis.x, deg2rad(jump_angle))
	force_dir.x *= -1 # Specchio il vettore orizzontalmente
	
	if Debug.is_category_active("movement"):
		DrawLine3d.DrawLine(player.global_transform.origin, player.global_transform.origin + force_dir * force, Color.blueviolet, 5)
	
	player.add_force(force_dir * force)


func move(delta):
	player.set_horizontal_velocity(Vector3.ZERO)
	player.apply_gravity(delta)
	player.move_and_snap()


func has_to_wall_jump() -> bool:
	var pl_pos: Vector3 = player.global_transform.origin
	var pl_basis: Basis = player.transform.basis
	var ray_length := 1.9
	var raycast: Dictionary = RaycastUtils.raycast(pl_pos, pl_pos + pl_basis.z * ray_length, [player])
	var is_angle_right: bool = raycast.has("position")
	var has_to_bounce: bool = player.is_on_wall() and is_angle_right
	
	return has_to_bounce and not player.is_on_floor() and player.input_system.is_just_pressed("jump")


func has_to_land() -> bool:
	var is_too_slow: bool = player._actual_velocity.length() < player.mass * 1.15
	return player._actual_velocity.y < 0 or is_too_slow
