extends Movement


export var walking_speed := 30


func act() -> void:
	movement_system.set_moving_direction_with_speed(walking_speed)
