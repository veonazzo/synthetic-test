extends Movement


export var shifting_speed := 10


func shift():
	_set_player_height(0.5)


func stand_up():
	_set_player_height(1)


func move():
	shift()
	movement_system.set_moving_direction_with_speed(shifting_speed)


func _set_player_height(height: float):
	player.mesh.scale.z = height
	player.collision.scale.y = height
