extends Spatial


export(NodePath) onready var target = get_node(target)

export var basis_x := true
export var basis_y := true
export var basis_z := true
export var position_x := true
export var position_y := true
export var position_z := true

var _did_target_move := false
var _previous: Transform
var _current: Transform


func _ready() -> void:
	set_as_toplevel(true)

	_previous = global_transform
	_current = global_transform
	
	_update_transform()

	_interpolate(1)


func _update_transform():
	var target_basis = target.global_transform.basis
	var target_position = target.global_transform.origin
	_previous = _current
	
	if basis_x:
		_current.basis.x = target_basis.x
	if basis_y:
		_current.basis.y = target_basis.y
	if basis_z:
		_current.basis.z = target_basis.z
	if position_x:
		_current.origin.x = target_position.x
	if position_y:
		_current.origin.y = target_position.y
	if position_z:
		_current.origin.z = target_position.z


func _process(_delta: float) -> void:
	var factor = clamp(Engine.get_physics_interpolation_fraction(), 0, 1)

	if _did_target_move:
		_update_transform()
		_did_target_move = false
	
	_interpolate(factor)


func _interpolate(factor: float) -> void:
	global_transform = _previous.interpolate_with(_current, factor)


func _physics_process(_delta: float) -> void:
	_did_target_move = true
