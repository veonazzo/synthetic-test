extends Node


export(NodePath) onready var dash = get_node(dash)
export(NodePath) onready var dodge = get_node(dodge)
export(NodePath) onready var jump = get_node(jump)
export(NodePath) onready var rotate = get_node(rotate)
export(NodePath) onready var shift = get_node(shift)
export(NodePath) onready var slide = get_node(slide)
export(NodePath) onready var walk = get_node(walk)
export(NodePath) onready var wall_jump = get_node(wall_jump)
onready var player = get_parent()


func is_far_from_floor() -> bool:
	var player_pos: Vector3 = player.global_transform.origin
	var max_distance := 15
	var result: Dictionary = RaycastUtils.raycast(player_pos, player_pos + (Vector3.DOWN * max_distance), [self])

	if result.has("position"):
		var distance = result["position"].distance_to(player_pos)
		return distance > max_distance

	return true


func get_input_direction() -> Vector3:
	var dir = Vector3.ZERO

	if player.input_system.is_pressed("backward"):
		dir -= player.transform.basis.z
	if player.input_system.is_pressed("forward"):
		dir += player.transform.basis.z
	if player.input_system.is_pressed("right"):
		dir -= player.transform.basis.x
	if player.input_system.is_pressed("left"):
		dir += player.transform.basis.x

	return dir.normalized()


func set_moving_direction_with_speed(speed: float):
	var dir: Vector3 = get_input_direction()
	var velocity: Vector3

	velocity = dir * speed

	player.set_horizontal_velocity(velocity)
