class_name InputSystemActions

var actions: Array
var category: String # u per unhandled, p per physics process


func _init(category:String) -> void:
	self.category = category
	Debug.Print("input", "InputSystemActions initialized")


func add_action(action) -> void:
	if actions.empty():
		actions.append(action)
	else:
		var size:int = actions.size()
		for i in range(size):
			if action.required_clicks <= actions[i].required_clicks:
				actions.insert(i,action)
				return
			if action.required_clicks > actions[i].required_clicks:
				actions.insert(i+1,action)
				return


func _to_string() -> String:
	var final_string:String = "Category: " + self.category
	final_string += "\nActions:"
	for action in self.actions:
		final_string += "\n    "
		final_string += action._to_string()
	return final_string


func get_action_by_name(action_name:String):
	for action in actions:
		if action.name == action_name:
			return action
	return null
