extends Node


var _input_system 


func _init() -> void:
	_input_system = InputSystemClass.new()


func register_function(actions_array:Array, state_name:String, function_ref:FuncRef) -> void:
	_input_system.register_function(actions_array, state_name, function_ref)


func _unhandled_input(event) -> void:
	if not event is InputEventMouseMotion:
		_input_system._unhandled_input()

 
func _physics_process(delta) -> void:
	_input_system._physics_process()


func get_state_status(action:String, state:String) -> bool:
	return _input_system.get_state_status(action, state)


func is_just_pressed(action:String) -> bool:
	return get_state_status(action, "just_pressed")


func is_pressed(action:String) -> bool:
	return get_state_status(action, "pressed")


func is_just_released(action:String) -> bool:
	return get_state_status(action, "just_released")


func action_press(action:String) -> void:
	_input_system.action_press(action)


func action_release(action:String) -> void:
	_input_system.action_release(action)
