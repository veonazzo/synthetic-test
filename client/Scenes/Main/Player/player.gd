extends CustomKinematicBody

export(NodePath) onready var network_manager = get_node(network_manager)
export(NodePath) onready var energy = get_node(energy)
export(NodePath) onready var health = get_node(health)
export(NodePath) onready var movement = get_node(movement)
export(NodePath) onready var collision = get_node(collision)
export(NodePath) onready var movement_state_machine = get_node(movement_state_machine)
export(NodePath) onready var mesh = get_node(mesh)
export(NodePath) onready var input_system = get_node(input_system)
export(NodePath) onready var weapon_system = get_node(weapon_system)

#var InputSystem = preload("res://Scenes/Main/Player/InputSystem/InputSystem.gd").new()


var packet_limit: float = 1.0/15.0
var limit: float


func _physics_process(_delta):
	Debug.Print("player", "Energy: " + str(energy._energy))
	Debug.Print("player", "Health: " + str(health._health))
	if movement_state_machine.state != null:
		Debug.Print("player", "State: " + str(movement_state_machine.state))


func _process(delta):
	if network_manager.connection_state == ConnectionState.CONNECTED:
		if (limit > packet_limit):
			_send_inputs()
			limit = 0
		limit += delta


func _send_inputs() -> void:

	var weapon: int


	weapon = weapon_system.current
	var rotation_degrees: Vector3 = get_rotation_degrees()
	#print(rotation_degrees)
	rotation_degrees.x = _degree_to_short(rotation_degrees.x)
	rotation_degrees.y = _degree_to_short(rotation_degrees.y)
	rotation_degrees.z = _degree_to_short(rotation_degrees.z)
	#print(rotation_degrees)
	network_manager.send_cpacketinput(
		rotation_degrees,
		weapon
	)
	#print(input_system._input_system._inputs)
	while input_system._input_system._inputs.size() != 0:
		network_manager.send_cpacketactions(
			input_system._input_system._inputs.pop_front()
		)
		
	
#Temp function, move away
func _degree_to_short(degree: int) -> int:
	return (int((degree)*65536/360) & 65535)
