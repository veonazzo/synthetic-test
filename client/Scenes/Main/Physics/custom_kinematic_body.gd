extends KinematicBody
class_name CustomKinematicBody


export var mass: float # quanto le forze avranno effetto.

onready var _default_fall_acceleration: float = mass / 2
onready var _default_acceleration: float = mass  * 0.44

var _desired_velocity := Vector3.ZERO
var _actual_velocity := Vector3.ZERO


func move_and_snap() -> void:
	var floor_normal := get_floor_normal()
	var velocity := _actual_velocity
	
	_actual_velocity = move_and_slide_with_snap(velocity, -floor_normal, Vector3.UP, true)


func set_velocity(new_velocity: Vector3):
	_desired_velocity = new_velocity
	_actual_velocity = new_velocity


func set_desired_velocity(new_velocity: Vector3) -> void:
	_desired_velocity = new_velocity


func set_horizontal_velocity(new_velocity: Vector3, desired_velocity: bool = true) -> void:
	_desired_velocity.x = new_velocity.x
	_desired_velocity.z = new_velocity.z
	
	if not desired_velocity:
		_actual_velocity.x = _desired_velocity.x
		_actual_velocity.z = _desired_velocity.z


func move_horizontally(delta: float, acceleration: float = _default_acceleration) -> void:
	acceleration = clamp(acceleration * delta, 0, 1)

	_actual_velocity.x = _actual_velocity.linear_interpolate(_desired_velocity, acceleration).x
	_actual_velocity.z = _actual_velocity.linear_interpolate(_desired_velocity, acceleration).z
	
	move_and_snap()


func add_force(force: Vector3) -> void:
	_actual_velocity += force


func apply_gravity(delta: float, fall_acceleration: float = _default_fall_acceleration) -> void:
	var max_fall_acceleration_multiplier := 14
	var max_falling_velocity: float = -max_fall_acceleration_multiplier * mass
	fall_acceleration = _multiply_fall_acceleration(fall_acceleration)
	
	_actual_velocity.y -= fall_acceleration * delta
	_actual_velocity.y = clamp(_actual_velocity.y, max_falling_velocity, _actual_velocity.y)


func has_to_fall() -> bool:
	return not is_on_floor()


func is_not_moving() -> bool:
	var velocity := _actual_velocity
	
	if is_on_floor():
		velocity.y = 0
		
	return velocity.length() < mass



func _multiply_fall_acceleration(fall_acceleration: float) -> float:
	var gravity_multiplier := 45.0
	return gravity_multiplier * fall_acceleration
