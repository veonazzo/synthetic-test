extends Node


export(NodePath) onready var player = get_node(player)


func _process(delta):
	$NewState.text = str(player.movement_state_machine.state)
	$OldState.text = str(player.movement_state_machine.previous_state)
