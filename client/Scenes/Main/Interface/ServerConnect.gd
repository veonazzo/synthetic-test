extends Button

var _server_ip_field: TextEdit
var _server_port_field: TextEdit


func _ready():
	_server_ip_field = get_node("/root/Client/Control/ServerIP")
	_server_port_field = get_node("/root/Client/Control/ServerPort")
	connect("pressed", self, "_button_pressed")


func _button_pressed():
	var network_manager =  get_node("/root/Client/Networking/NetworkManager")
	network_manager.init(_server_ip_field.text, int(_server_port_field.text))
	_server_ip_field.release_focus()
	_server_port_field.release_focus()
	release_focus()
