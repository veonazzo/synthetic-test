extends Button

var _access_token_field: TextEdit

func _ready():
	_access_token_field = get_node("/root/Client/Control/NakamaLogin/AccessToken")
	connect("pressed", self, "_button_pressed")


func _button_pressed():
	var nakama = get_node("/root/Client/Networking/Nakama")
	nakama.init(_access_token_field.text)
