extends ProgressBar

export(NodePath) onready var player = get_node(player)


func _process(delta):
	value = player.health._health
